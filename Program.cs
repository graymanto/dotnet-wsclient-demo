﻿using System;
using System.Net.WebSockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace WebsocketClient
{
    class Program
    {
        static async Task Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            var client = new ClientWebSocket();
            await client.ConnectAsync(new Uri("ws://localhost:8585"), CancellationToken.None);

            string message = "Ping";

            var buffer = new ArraySegment<byte>(Encoding.UTF8.GetBytes(message));

            await client.SendAsync(buffer, WebSocketMessageType.Text,
                true, CancellationToken.None);

            var receiveBufferSize = 1536;
            var receiveBuffer = new byte[receiveBufferSize];
            var result = await client.ReceiveAsync(new ArraySegment<byte>(receiveBuffer),
                CancellationToken.None);
            var stringResult = (new UTF8Encoding()).GetString(receiveBuffer);

            Console.WriteLine("Received " + stringResult);
        }
    }
}
